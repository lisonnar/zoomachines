﻿using UnityEngine;
using System.Collections;

public class AutoLoadSceneAfter : MonoBehaviour {

    public float time =3f;
    public LoadScene sceneToLoad;
    public bool withDelay=true;
	void Start () {
        Invoke("LoadScene",time);
	}
	
	void LoadScene () {
        if(sceneToLoad!=null)
        sceneToLoad.LoadSelectedSceneWithDelay();
	
	}
}
