﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WalkerRandom : Walker {

	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		base.Update ();
	}

	public override void FindNextPoint ()
	{
		List<Transform> availableGoals = new List<Transform> (goals);

		availableGoals.Remove (_actualGoal);
		availableGoals.RemoveAll (t => GoalIsBehind (gameObject.transform, t));
		//availableGoals.RemoveAll (t=> ToFarAway(gameObject.transform, t));

		if (availableGoals.Count > 0) 
		{
			_actualGoal = availableGoals [Random.Range (0, availableGoals.Count)];

			_navAgent.ResetPath();
			_navAgent.SetDestination(_actualGoal.position);
		} 
		else 
		{
			availableGoals = new List<Transform>(goals);
			availableGoals.Remove(_actualGoal);

			//availableGoals.RemoveAll(t => ToClose(gameObject.transform, t));

			Transform choosenGoal = null;
			float maxDistance = 0f;

			foreach(Transform t in goals)
			{
				float distance = Vector3.Distance(transform.position, t.position);
				if (distance > maxDistance)
				{
					choosenGoal = t;
					maxDistance = distance;
				}
			}

			_actualGoal = choosenGoal;

			//_actualGoal = availableGoals [Random.Range (0, availableGoals.Count)];

			_navAgent.ResetPath();
			_navAgent.SetDestination(choosenGoal.position);
		}
	}

	private bool GoalIsBehind (Transform agent, Transform goal)
	{
		Vector3 heading = goal.position - agent.position;

		return Vector3.Angle (agent.forward, heading) > 100f;
	}

	/*
	private bool ToFarAway (Transform agent, Transform goal)
	{
		return Vector3.Distance(agent.position, goal.position) > 20f;
	}
	*/
	
	private bool ToClose (Transform agent, Transform goal)
	{
		return Vector3.Distance (agent.position, goal.position) < 15f;
	}
}
