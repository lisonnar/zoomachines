﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Walker : MonoBehaviour 
{
	protected Transform		_actualGoal 	 = null;
	protected NavMeshAgent	_navAgent		 = null;
	protected int			_actualGoalIndex = 0;

	public	List<Transform>	goals			 = new List<Transform>();

	public virtual void Start () 
	{
		_actualGoal = goals [_actualGoalIndex];

		_navAgent = GetComponent<NavMeshAgent>();
		_navAgent.destination = _actualGoal.position;
	}
	
	// Update is called once per frame
	public virtual void Update () 
	{
		if (_navAgent.remainingDistance < 0.5f && Vector3.Distance(transform.position, _actualGoal.position) < 0.5f) 
		{
			FindNextPoint();
		}
	}

	public virtual void FindNextPoint ()
	{
		_actualGoalIndex ++;
		
		if (_actualGoalIndex == goals.Count)
		{	
			_actualGoalIndex = 0;
		}
		
		_actualGoal = goals[_actualGoalIndex];
		
		_navAgent.destination = _actualGoal.position;
	}
}
