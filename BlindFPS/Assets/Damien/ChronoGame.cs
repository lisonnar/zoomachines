﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using UnityEngine.Audio;

public class ChronoGame : MonoBehaviour
{
	public LoadScene loadScene;

	public float chrono = 180f;
	public int part = 3;

	public AudioClip Music1 = null;
	public AudioClip Music2 = null;
	public AudioClip Voice1 = null;
	public AudioClip Voice2 = null;
	public AudioClip Voice3 = null;
	public AudioClip VoiceGameOver = null;
	public AudioClip VoiceWin	   = null;

	public Material WallMaterial = null;

	private float startTimer = 0f;

	private int timeEffectIndex = 0;
	private float[] timeEffect = null;

	public Action[] timeActions = null;

	private bool gameOver = false;

	public AudioMixer mixer = null;

	public AudioMixerSnapshot snapshotMusic1;
	public AudioMixerSnapshot snapshotMusic2;
	public AudioMixerSnapshot snapshotFx;

	public AudioSource Audio1;
	public AudioSource Audio2;
	public AudioSource AudioFX;

	// Use this for initialization
	IEnumerator Start () 
	{
		snapshotMusic1 = mixer.FindSnapshot ("PlayMusic1");
		snapshotMusic2 = mixer.FindSnapshot ("PlayMusic2");
		snapshotFx 	   = mixer.FindSnapshot ("FX");

		WallMaterial.color = Color.black;

		yield return null;
		yield return null;

		GameStart ();
	}


	public void GameStart ()
	{
		WallMaterial.color = Color.white;
		
		timeEffect = new float[part];
		
		for (int i= 0; i < part; i++) 
		{
			timeEffect[i] = chrono * ( (i+1) / (float)part );
		}
		
		timeActions = new Action[3];
		timeActions [0] = OnTimer1Reach;
		timeActions [1] = OnTimer2Reach;
		timeActions [2] = OnGameOver;
		
		startTimer = Time.time;

		OnGameStart ();
	}

	// Update is called once per frame
	void Update () 
	{
		if (gameOver==true)
			return;

		float time = Time.time - startTimer;

		if (time >= timeEffect [timeEffectIndex]) 
		{
			timeActions[timeEffectIndex]();
			timeEffectIndex ++;
		}
	}

	public void OnGameStart ()
	{
		Debug.Log ("Game Start");

		AudioFX.PlayOneShot (Voice1);
	}

	public void OnTimer1Reach ()
	{
		Debug.Log ("time 1 ");

		AudioFX.PlayOneShot (Voice2);
	}

	public void OnTimer2Reach ()
	{
		Debug.Log ("time 2");
		WallMaterial.color = Color.black;

		AudioFX.PlayOneShot (Voice3);

		mixer.TransitionToSnapshots(
			new AudioMixerSnapshot[]{snapshotMusic2},
		new float[]{1f},
		1.5f);
	}

	public void OnWin ()
	{
		gameOver = true;
		AudioFX.PlayOneShot (VoiceWin);
		loadScene.LoadSelectedSceneWithDelay ();	
	}

	public void OnGameOver()
	{
		gameOver = true;
		AudioFX.PlayOneShot (VoiceGameOver);
		loadScene.LoadSelectedSceneWithDelay ();
	}
}
