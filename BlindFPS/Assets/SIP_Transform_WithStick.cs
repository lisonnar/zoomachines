﻿using UnityEngine;
using System.Collections;

public class SIP_Transform_WithStick : SIP_Transform {

    public StickData stickdata;

    public override void Start() {
        base.Start();
        stickdata = GameObject.FindObjectOfType<StickData>();

    }
    public override bool IsOneCursorsActive() {
        bool isPressingWithStick = stickdata != null && stickdata.isPressingWithHand;
        return isPressingWithStick || Input.GetButton("Fire1") || base.IsOneCursorsActive();
    
    }
	
}
