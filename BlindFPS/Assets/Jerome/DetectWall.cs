﻿using UnityEngine;
using System.Collections;

public class DetectWall : MonoBehaviour
{


	public ParticleSystem collisionParticles;


	void Start ()
	{
		collisionParticles.emissionRate = 0f;
	}
	
	private void OnTriggerEnter(Collider Other)
	{
		if(Other.gameObject.tag == "Wall")
		{
			collisionParticles.emissionRate = 30f;
		}
	}

	private void OnTriggerExit(Collider Other)
	{
		if(Other.gameObject.tag == "Wall")
		{
			collisionParticles.emissionRate = 0f;
		}
	}
}
