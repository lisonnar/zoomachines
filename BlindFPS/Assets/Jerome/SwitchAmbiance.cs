﻿using UnityEngine;
using System.Collections;

public class SwitchAmbiance : MonoBehaviour
{
	public Material unlit;

	public GameObject musiqueDark;
	public GameObject musiqueLight;

	/*public AudioClip dark;
	public AudioClip light;*/

	private Color curentColor;
	private bool stateAmbiance;

	private float timeToWait = 30f;
	private float currentTime;
	private bool isCoolingDown;


    public BlindState blindGameState;




	void Start ()
	{
		unlit.color = new Color (1.0f,1.0f,1.0f,0.0f);
		stateAmbiance = false;
		musiqueDark.SetActive(false);
		musiqueLight.SetActive(true);
		currentTime = timeToWait;
		if (blindGameState == null) blindGameState = GameObject.FindObjectOfType<BlindState>();
		SwitchB();

	        
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			stateAmbiance = !stateAmbiance;

			if(stateAmbiance == true & isCoolingDown == false)
			{
				SwitchA();
				isCoolingDown = true;
				//musique.Play();
			}
			
			if(stateAmbiance == false & isCoolingDown == false)
			{
				SwitchB();
				//musique.Play();
				isCoolingDown = true;
			}
		}


	}
	void Update ()
	{
		if(isCoolingDown)
		{
			currentTime -= Time.deltaTime;
		}

		if(currentTime <= 0f)
		{
			currentTime = timeToWait;
			isCoolingDown = false;

		}


	}

	void SwitchA()
	{	
		unlit.color = new Color (0.0f,0.0f,0.0f,0.0f);
		musiqueLight.SetActive(false);
		musiqueDark.SetActive(true);
	        if (blindGameState)
        {
            blindGameState.MusicPlayed = "DaftPunk";
            blindGameState.BackGroundColor = unlit.color;
        }



	}

	void SwitchB()
	{
		unlit.color = new Color (1.0f,1.0f,1.0f,0.0f);
		musiqueDark.SetActive(false);
		musiqueLight.SetActive(true);
        if (blindGameState)
        {
            blindGameState.MusicPlayed = "ErikSatie";
            blindGameState.BackGroundColor = unlit.color;
        }


	}
}
