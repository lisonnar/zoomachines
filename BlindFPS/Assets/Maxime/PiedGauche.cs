﻿using UnityEngine;
using System.Collections;

public class PiedGauche : MonoBehaviour 
{
	public GameObject empreinte;
	GameObject empreinteclone;
	public GameObject pied;
	private float currenttime;
	public float timetowait;
	public float timebeforestart;



	// Use this for initialization
	void Start () 
	{


		currenttime = timebeforestart;

			
	}
	
	// Update is called once per frame
	void Update () 
	{
		currenttime -= Time.deltaTime;
		if (currenttime <= 0f)
		{
			Empreintepop();
			currenttime = timetowait;
		}
	
	}
	void Empreintepop ()
	{	
		empreinteclone=Instantiate(empreinte,transform.position,pied.transform.rotation) as GameObject;

	}

}
