﻿using UnityEngine;
using System.Collections;

public class BlindState : MonoBehaviour {

    [SerializeField]
    private Color _backGroundColor;

    public Color BackGroundColor
    {
        get { return _backGroundColor; }
        set {  bool isNewValue = value != _backGroundColor; _backGroundColor = value; if (isNewValue) RefreshState(); }
    }

    [SerializeField]
    private string _musicPlayed;
    public string MusicPlayed
    {
        get { return _musicPlayed; }
        set { bool isNewValue = value!=_musicPlayed; _musicPlayed = value; if(isNewValue) RefreshState(); }
    }
    
    public UDP_LinkSenderToServer serverLink;

	void Start () {
        InvokeRepeating("RefreshState", 0, 1f);
	
	}   

    public void RefreshState() {

        if (serverLink.IsConnected())
        {
            serverLink.sender.Send("M:" + _musicPlayed);
            serverLink.sender.Send(string.Format("Col:{0}:{1}:{2}:{3}", _backGroundColor.r, _backGroundColor.g, _backGroundColor.b, _backGroundColor.a));
        
        }
    }
	
}
