﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.

public class ButtonUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private int moveHeight = 10;

    public GameObject top1;
    public GameObject top2;
    public GameObject top3;
    public GameObject bottom1;
    public GameObject bottom2;
    public GameObject bottom3;

	Vector3 vecto;

	public GameObject mover;

	public Color color;
	public Image forme;

    public GameObject text;
    public GameObject line;

    //Do this when the cursor enters the rect area of this selectable UI object.
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("On");

        if (top1 != null)
        {
            vecto = top1.transform.localPosition;
            vecto.y += moveHeight;
            top1.transform.localPosition = vecto;
        }

        if (top2 != null)
        {
            vecto = top2.transform.localPosition;
            vecto.y += moveHeight;
            top2.transform.localPosition = vecto;
        }

        if (top3 != null)
        {
            vecto = top3.transform.localPosition;
            vecto.y += moveHeight;
            top3.transform.localPosition = vecto;
        }

        if (bottom1 != null)
        {
            vecto = bottom1.transform.localPosition;
            vecto.y -= moveHeight;
            bottom1.transform.localPosition = vecto;
        }

        if (bottom2 != null)
        {
            vecto = bottom2.transform.localPosition;
            vecto.y -= moveHeight;
            bottom2.transform.localPosition = vecto;
        }

        if (bottom3 != null)
        {
            vecto = bottom3.transform.localPosition;
            vecto.y -= moveHeight;
            bottom3.transform.localPosition = vecto;
        }
        
		vecto = mover.transform.localPosition;
		vecto.x -= moveHeight*2;
		mover.transform.localPosition = vecto;

		forme.color = color;

        Invoke("ViewLine", .1f);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("Off");

        if (top1 != null)
        {
            vecto = top1.transform.localPosition;
            vecto.y -= moveHeight;
            top1.transform.localPosition = vecto;
        }

        if (top2 != null)
        {
            vecto = top2.transform.localPosition;
            vecto.y -= moveHeight;
            top2.transform.localPosition = vecto;
        }

        if (top3 != null)
        {
            vecto = top3.transform.localPosition;
            vecto.y -= moveHeight;
            top3.transform.localPosition = vecto;
        }

        if (bottom1 != null)
        {
            vecto = bottom1.transform.localPosition;
            vecto.y += moveHeight;
            bottom1.transform.localPosition = vecto;
        }

        if (bottom2 != null)
        {
            vecto = bottom2.transform.localPosition;
            vecto.y += moveHeight;
            bottom2.transform.localPosition = vecto;
        }

        if (bottom3 != null)
        {
            vecto = bottom3.transform.localPosition;
            vecto.y += moveHeight;
            bottom3.transform.localPosition = vecto;
        }

		vecto = mover.transform.localPosition;
		vecto.x += moveHeight*2;
		mover.transform.localPosition = vecto;

		forme.color = new Color(255,255,255);

        ViewLine();

    }

    void ViewLine()
    {
        if (line.activeSelf)
        {
            line.SetActive(false);

            ViewText();
        }
        else
        {
            line.SetActive(true);
            Invoke("ViewText", .1f);
        }

    }

    void ViewText()
    {
        if (text.activeSelf)
        {
            text.SetActive(false);
        }
        else
        {
            text.SetActive(true);
        }
    }
}