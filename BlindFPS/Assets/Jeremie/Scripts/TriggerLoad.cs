﻿using UnityEngine;
using System.Collections;

public class TriggerLoad : MonoBehaviour {

    public LoadScene sceneLoader;

	void OnTrigger()
    {
        sceneLoader.LoadSelectedSceneWithDelay();
    }
}
