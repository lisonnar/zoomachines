﻿using UnityEngine;
using System.Collections;

public class MoveWithUnityInput : MonoBehaviour {


    public Transform objectToMove;
    public Transform refDirection;
    public float fowardSpeed = 2f;
    public float lateralSpeed = 1f;


    void Update()
    {

        objectToMove.position += refDirection.forward * Input.GetAxis("Vertical") * (Time.deltaTime * fowardSpeed);
        objectToMove.position += refDirection.right * Input.GetAxis("Horizontal") * (Time.deltaTime * lateralSpeed);
    }
}
