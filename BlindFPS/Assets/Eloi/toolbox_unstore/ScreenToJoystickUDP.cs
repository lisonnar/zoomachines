﻿using UnityEngine;
using System.Collections;

public class ScreenToJoystickUDP : MonoBehaviour {

    public UDP_LinkSenderToServer serverLink;

    public float minDistanceToMove = 0.2f;
    public float maxDistanceToMove = 0.5f;


    private bool _isPressing;

    public bool IsPressing
    {
        get { return _isPressing; }
        set {
            if (_isPressing != value)
            {
                if (value) StartPressing();
                else StopPressing();
            }
            _isPressing = value;
                
        }
    }

    public Vector2 startPression;
    public Vector2 currentPression;
    public bool _isSonarActive;
    public Vector2 _joystick;




    void Start () {

        Input.compass.enabled = true;
        //Input.gyro.enabled = true;
        InvokeRepeating("SendJoystickData", 0, 0.1f);
	}

    public Vector2 GetTouchPositionInPourcent()
    {
        if(Input.touchCount<=0)return Vector2.zero;
        Vector2 result = Input.touches[0].position;
        result.x /= Screen.width;
        result.y /= Screen.height;
        return result;
    }

    void Update() {

        IsPressing = Input.touchCount > 0;

        if (Input.touches.Length>0)
           currentPression=  Input.touches[0].position;
        

        if (startPression != Vector2.zero && currentPression != Vector2.zero)
        {
            Vector2 direction = currentPression - startPression;
            if (direction.magnitude > minDistanceToMove)
            {

                float pctPower = Mathf.Clamp( (direction.magnitude - minDistanceToMove) / (maxDistanceToMove - minDistanceToMove),0f,1f);
                _joystick = direction.normalized*pctPower;
                _isSonarActive = Input.touchCount>1;

            }
            else
            {
                _joystick = Vector3.zero;
                _isSonarActive = true;
            }

        }
        else {
            _joystick = Vector2.zero;
            _isSonarActive = false;
        } 


    
    }

    void SendJoystickData ()
    {
        if (serverLink.IsConnected()) {
            serverLink.sender.Send(string.Format("JoXY:{0:0.00}:{1:0.00}", _joystick.x, _joystick.y));
            serverLink.sender.Send(string.Format("So:{0}", _isSonarActive ? 1 : 0));
            serverLink.sender.Send(string.Format("Comp:{0}", Input.compass.magneticHeading));
            Vector3 ac = Input.acceleration;
            serverLink.sender.Send(string.Format("Acc:{0}:{1}:{2}", ac.x, ac.y, ac.z));
        }
        
    
    }


    private void StopPressing()
    {

        startPression = Vector2.zero;
    }

    private void StartPressing()
    {
        startPression = Input.touches[0].position;
    }
    
}
