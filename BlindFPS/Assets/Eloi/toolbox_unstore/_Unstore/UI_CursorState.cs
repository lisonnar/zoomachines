﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_CursorState : MonoBehaviour {


    public float _pourcentLoad;

    public float PourcentLoaded
    {
        get { return _pourcentLoad; }
        set {
            _pourcentLoad = Mathf.Clamp(value,0f,1f);
            SetImageState(_pourcentLoad);
            
        
        }
    }

    public void OnEnable() {
        PourcentLoaded = 0;
    
    }
    public Image centerZone;
    public Image circleZone;

    public void SetImageState(float pourcent) {
        pourcent = Mathf.Clamp(pourcent, 0f, 1f);
        circleZone.fillAmount = pourcent;

        float pourcentColorAlpha=0f;
        Color buttonColor = centerZone.color;

        if (pourcent <= 0.75f)
            pourcentColorAlpha = 0f;
        else pourcentColorAlpha = (pourcent - 0.75f) * 4f;
        pourcentColorAlpha = Mathf.Clamp(pourcentColorAlpha, 0f, 1f);
        buttonColor.a = pourcentColorAlpha;
        centerZone.color = buttonColor;
            
        
    }

}
