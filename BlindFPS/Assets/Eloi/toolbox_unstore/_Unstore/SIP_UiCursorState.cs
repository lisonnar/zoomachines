﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SIP_UiCursorState : MonoBehaviour {

    public enum TimeLeft { HoverSince, ActiveSince }
    public TimeLeft timeleft = TimeLeft.HoverSince;
    public UI_CursorState uiCursor;
	
    
    void Update () {
        ScreenToIngameInteraction currentSI = ScreenToIngameInteraction.InstanceInScene;
  
        if (!currentSI) { uiCursor.PourcentLoaded=0; return; }
        GameObject selected = currentSI.SelectedObject;
        if (!selected) { uiCursor.PourcentLoaded = 0; return; }
        IScreenInteractionCustom button = selected.GetComponent<IScreenInteractionCustom>() as IScreenInteractionCustom;
        if (button == null) { uiCursor.PourcentLoaded = 0; return; }

            if (currentSI.SelectedObject)
                uiCursor.PourcentLoaded = 1f - (timeleft == TimeLeft.HoverSince ? currentSI.GetTimeLeft_StayHover() : currentSI.GetTimeLeft_StayActived());
            else uiCursor.PourcentLoaded = 0;
	
	}
}
