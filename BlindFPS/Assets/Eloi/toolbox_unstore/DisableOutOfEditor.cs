﻿using UnityEngine;
using System.Collections;

public class DisableOutOfEditor : MonoBehaviour {

	// Use this for initialization
	void Awake () {
	
#if ! UNITY_EDITOR 
        gameObject.SetActive(false);
#endif
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
