﻿using UnityEngine;
using System.Collections;

public class StopEmission : MonoBehaviour
{
	public ParticleSystem pinkJet;

	public float timeToLive=6f;
	public float countDown = 0;
	
	void Start ()
	{
		pinkJet.enableEmission = true;
	}

	void Update ()
	{
		if (countDown <= 0f)
			return;

		countDown -= Time.deltaTime;

		if (countDown <= 0f)
		{
			pinkJet.enableEmission = false;
		}

	}
}
