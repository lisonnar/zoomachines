﻿using UnityEngine;
using System.Collections;

public class EscapeToScene : MonoBehaviour {


    public LoadScene loadScene;

    void Update () {

        if (loadScene!=null &&  Input.GetKeyDown(KeyCode.Escape)) {
            loadScene.LoadSelectedScene();
            //Destroy(this);
            return;
        }
	}
}
