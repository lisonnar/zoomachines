﻿using UnityEngine;
using System.Collections;

public class MoveObjectAtSimpleRaycastPosition : MonoBehaviour {

    public Transform objectToMove;
    public RayCastSimpleObject raycast;


    void Update()
    {
        if(raycast.hasCollided){
            objectToMove.position = raycast.collisionPoint;
            objectToMove.forward = raycast.collisionDirection;
        }
        
	}
}
