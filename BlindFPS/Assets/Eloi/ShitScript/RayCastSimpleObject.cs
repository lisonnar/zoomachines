﻿using UnityEngine;
using System.Collections;

public class RayCastSimpleObject : MonoBehaviour
{


    public Transform viewRef;
    public Vector3 viewPosition;
    public bool hasCollided;
    public Vector3 collisionPoint;
    public Vector3 collisionDirection;
    public LayerMask allowCollision;


	
	void Update () {
        RefeshPosition();
	
	}

    private void RefeshPosition()
    {
        viewPosition = viewRef.position;
        Ray ray = new Ray(viewPosition, viewRef.forward);
        RaycastHit rayhit;
        hasCollided =Physics.Raycast(ray, out rayhit, float.MaxValue, allowCollision);
        if (hasCollided)
        {
            collisionPoint = rayhit.point;
            collisionDirection = viewRef.forward;
        }
    }
}
