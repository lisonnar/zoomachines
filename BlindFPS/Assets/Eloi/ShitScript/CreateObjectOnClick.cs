﻿using UnityEngine;
using System.Collections;

public class CreateObjectOnClick : MonoBehaviour {

    public string pinkKeyName = "PinkBullets";
    public PoolGenerator pinkBulletPool;
    public Transform where;
    public float lifeTime=1f;   

    public StickData stickData;
    public float cooldown=0.5f;
    private float cooldownCount;


    public void Start() {

        if (stickData == null) {
            stickData = GameObject.FindObjectOfType<StickData>();
        }
        if (pinkBulletPool == null)
            pinkBulletPool = PoolGenerator.GetPool(pinkKeyName);
        if (where == null)
            where = Camera.main.transform;
    }


    public void Update() {


        if (cooldownCount > 0f) {
            cooldownCount -= Time.deltaTime;
            return;
        }

		if (Input.GetMouseButton(0) ||Input.GetButton("Fire1") || (stickData != null && stickData.isPressingWithHand))
        {
            cooldownCount = cooldown;
            InstanciateObject();
        }
    }

    private void InstanciateObject()
    {
        GameObject created = pinkBulletPool.GetNextAvailable();
        created.SetActive(true);
        created.transform.forward = where.forward;
        created.transform.position = where.position;
        //LifeTime life = created.AddComponent<LifeTime>();
        //life.timeToLive = lifeTime;
    }


}
