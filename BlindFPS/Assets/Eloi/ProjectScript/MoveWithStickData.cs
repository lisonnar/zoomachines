﻿using UnityEngine;
using System.Collections;

public class MoveWithStickData : MonoBehaviour {

    public CharacterController charactController;
    public Transform objectToMove;
    public Transform refDirection;
    public float fowardSpeed=2f;
    public float lateralSpeed=1f;

    public StickData stickData;

    void Start() {
        if (stickData == null)
            stickData = GameObject.FindObjectOfType<StickData>();

		if (stickData == null)
			DestroyImmediate(this);
    }

	void Update () {

        if (stickData == null) 
            return;

        Vector3 direction = Vector3.zero;
        //Burk for the stick+ axis. but not time.
        if (stickData)
        {
			direction += refDirection.forward * (stickData.direction.y + Input.GetAxis("Vertical")) * (Time.deltaTime * fowardSpeed);
			direction += refDirection.right * (stickData.direction.x + Input.GetAxis("Horizontal")) * (Time.deltaTime * lateralSpeed);

        }
        else
        {
            direction += refDirection.forward * ( Input.GetAxis("Vertical")) * (Time.deltaTime * fowardSpeed);
			direction += refDirection.right * ( Input.GetAxis("Horizontal")) * (Time.deltaTime * lateralSpeed);
        
        }
        

            charactController.Move(direction);
            charactController.Move( direction);
	}
}
