﻿using UnityEngine;
using System.Collections;

public class StickDataFromUDP : MonoBehaviour {


    public LoadScene menuEscape;
    public StickData stick;
    public UDP_Server server;

    private Vector2 lastJoystickDirection;

	// Use this for initialization
	void Start () {
        server.receiver.onPackageReceived += TranslateUdpToStick;
	}

    public void Update() {

        stick.direction = Vector3.MoveTowards(stick.direction, lastJoystickDirection, Time.deltaTime);
    
    }

    private void TranslateUdpToStick(UDP_Receiver from, string message, string adresse, int port)
    {
            string[] tokens = message.Split(':');
        
            if (tokens.Length >= 3 && tokens[0].Equals("Joy"))
            {
                lastJoystickDirection = new Vector2(float.Parse(tokens[1]), float.Parse(tokens[2]));
            }
            if (tokens.Length >= 2 && tokens[0].Equals("Press"))
            {
                stick.SetHandPressingTo(int.Parse(tokens[1]) != 0);
            } 
            if (tokens.Length >= 2 && tokens[0].Equals("SecPress"))
            {
                stick.isSecondPressingOn =int.Parse(tokens[1]) != 0;
            }

            if (tokens.Length >= 1 && tokens[0].Equals("Esc"))
            {
                menuEscape.LoadSelectedSceneWithDelay();
            }

    }
	
}
