﻿using UnityEngine;
using System.Collections;

public class ChangeMusic : MonoBehaviour {

    public UdpToGameState gameState;
    public string currentMusic;
    public StringToMusic[] music;
    public AudioSource audioSource;

    public void Update() {
        if (currentMusic != gameState.MusicPlayed) {
            ChangeMusicPlayed(gameState.MusicPlayed);
            currentMusic = gameState.MusicPlayed;
        }
    
    }

    private void ChangeMusicPlayed(string musicName)
    {
        for (int i = 0; i < music.Length; i++)
        {
            if (musicName.Equals(music[i].musicId)) {
                audioSource.clip = music[i].audio;
                audioSource.Play();
                return;
            }
            
        }
    }
    
	

    [System.Serializable]
    public struct StringToMusic
    {
      public  string musicId;
      public  AudioClip audio;
    }
}
