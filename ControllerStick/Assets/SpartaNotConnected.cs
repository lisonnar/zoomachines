﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpartaNotConnected : MonoBehaviour {

    public UDP_LinkSenderToServer link;

    public StickData stickData;

    public GameObject leftSong;
    public GameObject rightSong;
    public GameObject frontSong;
    public GameObject backSong;

    public List<GameObject> created = new List<GameObject>();


	void Update () {

        if (link.IsConnected()) {
            Destroy(this);
            return ;
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (stickData.direction.y < -0.8f)
            {
                CreateObject(frontSong);

            }
            else if (stickData.direction.y > 0.8f)
            {

                CreateObject(backSong);
            }
            else if (stickData.direction.x < -0.8f)
            {

                CreateObject(leftSong);
            }
            else if (stickData.direction.x > 0.8f)
            {

                CreateObject(rightSong);
            }
            else {

                ClearAll();
            } 

        }
	
	}

    private void ClearAll()
    {
        for (int i = 0; i < created.Count; i++)
        {
            if (created[i] != null)
                DestroyImmediate(created[i]);
        }
        created.Clear();
    }

    private void CreateObject(GameObject song)
    {

        ClearAll();
      GameObject obj =  GameObject.Instantiate(song);
      LifeTime life = obj.GetComponent<LifeTime>();
      if (!life)
      {
          life = obj.AddComponent<LifeTime>();
          life.timeToLive = 10f;
      }
      created.Add(obj);
    }
}
