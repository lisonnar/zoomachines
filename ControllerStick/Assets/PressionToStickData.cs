﻿using UnityEngine;
using System.Collections;

public class PressionToStickData : MonoBehaviour {

    public StickData stickData;
    
    void Update () {

        stickData.isPressingWithHand = Input.GetMouseButton(0) || Input.touchCount>0;
        stickData.isSecondPressingOn = Input.GetMouseButton(1) || Input.GetMouseButton(2);
	
	}
}
