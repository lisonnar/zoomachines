﻿using UnityEngine;
using System.Collections;

public class DisableOnUDPServerFound : MonoBehaviour {



    public UDP_LinkSenderToServer serverLink;

    void Update () {
        if (serverLink.IsConnected())
            gameObject.SetActive(false);
	}
}
