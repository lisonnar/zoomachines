﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Warning durty code
/// </summary>
public class QuickFadeInOut : MonoBehaviour
{


	public Renderer fadeRenderer;
	public float fadeSpeedInPct = 1f;
	public enum FadeType { None, GoToFullColor, GoToTransparent }
	public FadeType fadeState;

	public Color currentColor;
	void OnEnable()
	{
		if (fadeRenderer)
			fadeRenderer.gameObject.SetActive(true);
		fadeState = FadeType.GoToTransparent;
	}

	void Update()
	{
		if (fadeState == FadeType.None) return;
		if (fadeRenderer)
			currentColor = fadeRenderer.material.color;

		if (fadeState == FadeType.GoToTransparent)
		{
			if (currentColor.a <= 0f)
				fadeState = FadeType.None;

			currentColor.a = Mathf.Clamp(currentColor.a - fadeSpeedInPct * Time.deltaTime, 0f, 1f);

		}
		else if (fadeState == FadeType.GoToFullColor)
		{

			if (currentColor.a >= 1f)
				fadeState = FadeType.None;

			currentColor.a = Mathf.Clamp(currentColor.a + fadeSpeedInPct * Time.deltaTime, 0f, 1f);
		}
		if (fadeRenderer)
			fadeRenderer.material.color = currentColor;
	}

	public void SetTransparent()
	{

		fadeState = FadeType.GoToTransparent;
	}
	public void SetBlack()
	{

		fadeState = FadeType.GoToFullColor;
	}

}
