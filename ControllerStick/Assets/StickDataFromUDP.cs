﻿using UnityEngine;
using System.Collections;

public class StickDataFromUDP : MonoBehaviour {


    public StickData stick;
    public UDP_Server server;

	// Use this for initialization
	void Start () {
        server.receiver.onPackageReceivedParallelThread += TranslateUdpToStick;
	}

    private void TranslateUdpToStick(UDP_Receiver from, string message, string adresse, int port)
    {
            string[] tokens = message.Split(':');
        
            if (tokens.Length >= 3 && tokens[0].Equals("Joy"))
            {
                stick.SetDirectionTo(float.Parse(tokens[1]), float.Parse(tokens[2]));
            }
            if (tokens.Length >= 2 && tokens[0].Equals("Press"))
            {
                stick.SetHandPressingTo(int.Parse(tokens[1])!=0);
            }
        
    }
	
}
