﻿using UnityEngine;
using System.Collections;

public class StickDataToUDP : MonoBehaviour {

    public float sendDelay = 0.1f;
    public StickData stickData;
    public UDP_LinkSenderToServer serverLink;
	
	// Update is called once per frame
	void Start () {
        InvokeRepeating("SendStickData", 0f, sendDelay);
	}

    public bool wasPressingWithHand;

    public void Update() {

        if (wasPressingWithHand != stickData.isPressingWithHand)
            SendStickData();

        wasPressingWithHand = stickData.isPressingWithHand;


    }

    public void SendStickData() {

        if (serverLink.IsConnected()) {
            serverLink.sender.Send(string.Format("Joy:{0:0.00}:{1:0.00}", stickData.direction.x, stickData.direction.y));
            serverLink.sender.Send(string.Format("Press:{0}", stickData.isPressingWithHand ? 1 : 0));
            serverLink.sender.Send(string.Format("SecPress:{0}", stickData.isSecondPressingOn ? 1 : 0));

            //Bad code last minute strart
            if (Input.touchCount >= 5)
            {
                Application.Quit();
            }

            else if (Input.touchCount >= 3)
            {
                    serverLink.sender.Send("Esc:");
                
            }

            //Bad code last minute end
        }
    }
}
