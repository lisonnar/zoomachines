﻿using UnityEngine;
using System.Collections;

public class UdpToGameState : MonoBehaviour {

    [SerializeField]
    private Color _backGroundColor;

    public Color BackGroundColor
    {
        get { return _backGroundColor; }
        set { _backGroundColor = value;  }
    }

    [SerializeField]
    private string _musicPlayed;
    public string MusicPlayed
    {
        get { return _musicPlayed; }
        set { _musicPlayed = value;  }
    }
    
    public UDP_Server server;



	void Start () {
        server.receiver.onPackageReceivedParallelThread+=UdpToGameSate;
	
	}

    private void UdpToGameSate(UDP_Receiver from, string message, string adresse, int port)
    {
        string [] tokens = message.Split(':');
        if(tokens.Length>=2 && tokens[0].Equals("M"))
            MusicPlayed = tokens[1];
        if(tokens.Length>=5 && tokens[0].Equals("Col"))
            BackGroundColor = new Color(float.Parse(tokens[1]),float.Parse(tokens[2]),float.Parse(tokens[3]),float.Parse(tokens[4]));

    }
	
}