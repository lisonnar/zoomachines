﻿using UnityEngine;
using System.Collections;

public class MoveWithStickData : MonoBehaviour {

    public Transform refDirection;
    public float fowardSpeed=2f;
    public float lateralSpeed=1f;

    public StickData stickData;

	void Update () {

        refDirection.position += refDirection.forward * stickData.direction.y *(Time.deltaTime *fowardSpeed);
        refDirection.position += refDirection.right * stickData.direction.x * (Time.deltaTime * lateralSpeed);
	}
}
