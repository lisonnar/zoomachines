﻿using UnityEngine;
using System.Collections;

public class MoveControllerWithAccelerometer : MonoBehaviour {

    public Transform targetToMove;
    public Vector2 joystick;
    public float speed=1f;
    
    [Range(0f,1f)]
    public float minToMove=0.1f;
    
    [Range(0f,1f)]
    public float maxToMOve=1f;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 acceleration =Input.acceleration;
        if (acceleration.x > -minToMove && acceleration.x < minToMove)
        {
            joystick.x = 0f;
        }
        else
        {

            joystick.x = ((Mathf.Abs(acceleration.x) - minToMove) / (maxToMOve - minToMove))  * Mathf.Sign(acceleration.x);
        }
        

        if (acceleration.z > -minToMove && acceleration.z < minToMove)
        {
            joystick.y = 0f;
        }
        else
        {
            joystick.y = ((Mathf.Abs(acceleration.z) - minToMove) / (maxToMOve - minToMove)) *-1f* Mathf.Sign(acceleration.z);
        }

        targetToMove.position += targetToMove.forward * joystick.y * (speed *Time.deltaTime);
        targetToMove.position += targetToMove.right * joystick.x * (speed * Time.deltaTime);



        if (Input.GetKey(KeyCode.LeftArrow))
            targetToMove.transform.Rotate(new Vector3(0, -90 * Time.deltaTime, 0));
        if (Input.GetKey(KeyCode.RightArrow))
            targetToMove.transform.Rotate(new Vector3(0, 90 * Time.deltaTime, 0));
	}

}
