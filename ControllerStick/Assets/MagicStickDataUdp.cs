﻿using UnityEngine;
using System.Collections;

public class MagicStickDataUdp : MonoBehaviour {

    public UDP_LinkSenderToServer serverLink;
    public MagicStickData magicStickData ;
    public Color lastColor;
   
	void Update () {
        if (lastColor != magicStickData.wantedColor && serverLink.IsConnected()) {
            lastColor = magicStickData.wantedColor;
            serverLink.sender.Send(string.Format("Col:{0}:{1}:{2}:{3}", lastColor.r,lastColor.g,lastColor.b,lastColor.a));
            
        }
	}
}
