﻿using UnityEngine;
using System.Collections;

public class StickData : MonoBehaviour {

    //Do the user want to move with stick
    public Vector2 direction;
    //Do the user is pressing with his hand a button
    public bool isPressingWithHand;
    public bool isSecondPressingOn;



    public void SetDirectionTo(float x, float y)
    {
        direction.x = x;
        direction.y = y;
    }

    internal void SetHandPressingTo(bool isPressing)
    {
        isPressingWithHand = isPressing;
    }
}
