﻿using UnityEngine;
using System.Collections;

public class AcceleroToStickData : MonoBehaviour {

    public StickData stickData;
    public Vector2 joystick;

    [Range(0f, 1f)]
    public float minToMove = 0.1f;

    [Range(0f, 1f)]
    public float maxToMOve = 1f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 acceleration = Input.acceleration;
        if (acceleration.x > -minToMove && acceleration.x < minToMove)
        {
            joystick.x = 0f;
        }
        else
        {

            joystick.x = ((Mathf.Abs(acceleration.x) - minToMove) / (maxToMOve - minToMove)) * -1f *  Mathf.Sign(acceleration.x);
        }


        if (acceleration.z > -minToMove && acceleration.z < minToMove)
        {
            joystick.y = 0f;
        }
        else
        {
            joystick.y = ((Mathf.Abs(acceleration.z) - minToMove) / (maxToMOve - minToMove))  * Mathf.Sign(acceleration.z);
        }

        stickData.SetDirectionTo(joystick.x,joystick.y);

	}
}
