﻿using UnityEngine;
using System.Collections;

public class ChangeCameraColor : MonoBehaviour {

    public UdpToGameState gameState;
    public Camera cameraColor;

	void Update () {
        if (cameraColor.backgroundColor != gameState.BackGroundColor) {
           cameraColor.backgroundColor = Color.Lerp(cameraColor.backgroundColor, gameState.BackGroundColor, Time.deltaTime);
        }
	}
}
